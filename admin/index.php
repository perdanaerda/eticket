<html>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<head>
  <link rel="stylesheet" type="text/css" href="../asset/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../asset/css/bootstrap-theme.min.css">
  <title>Tambah Film</title>
</head>
<body>
  <div class="container" align="center">
    <form class="form-horizontal" action="insert.php" method="POST" name="tambahfilm" enctype="multipart/form-data">
      <div class="row">
        <div class="form-group">
          <label class="control-label col-sm-5" for="kodefilm">Kode Film :</label>
            <div class="col-sm-4">
              <input type="text" name="kodefilm" class="form-control">
            </div>
        </div>
      </div>
      <div class="row">
        <div class="form-group">
          <label class="control-label col-sm-5" for="judul">Judul Film :</label>
          <div class="col-sm-7">
            <input type="text" name="judul" class="form-control">
          </div>
      </div>
       <div class="row">
         <div class="form-group">
          <label class="control-label col-sm-5" for="sinopsis">Sinopsis :</label>
          <div class="col-sm-7">
            <textarea class="form-control" name="sinopsis" rows="5" cols="1"></textarea>
          </div>
         </div>
       </div>
       <div class="row">
         <div class="form-group">
            <label class="control-label col-sm-5" for="gambar">Cover :</label>
          <div class="col-sm-7">
            <input type="file" name="gambar" id="gambar">
          </div>
         </div>
       </div>
        <div class="form-group">
         <button class="btn btn-success" type="submit">submit</button>
        </div>
    </form>
  </div>
</body>
</html>