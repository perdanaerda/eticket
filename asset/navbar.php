<!DOCTYPE html>
<html>
<head>
  <title>e-ticketing Bioskop</title>
  <?php include'asset/head.php';?>
  <style type="text/css">
    .navbar {
      padding-top: 15px;
      padding-bottom: 15px;
      border: 0;
      border-radius: 0;
      margin-bottom: 0;
      font-size: 12px;
      letter-spacing: 5px;
  }
  .navbar-nav  li a:hover {
      color: #1abc9c !important;
  }
  </style>
</head>
<body>
  <!-- Navbar -->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#"><strong>e-Ticketing</strong></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">HOW TO BOOK</a></li>
        <li><a href="#">COMING SOON MOVIE</a></li>
        <li><a href="#">THEATER</a></li>
        <li><a href="#">CONTACT US</a></li>
      </ul>
    </div>
  </div>
</nav>
</body>
</html>