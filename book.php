<!-- <?php 
	include 'asset/koneksi/koneksi.php';
	$kode = $_GET['kode'];
	$query = "SELECT * FROM tbl_film WHERE kode='$kode' LIMIT 1";
	$sql = mysqli_query($connect, $query);

	while ($data = mysqli_fetch_assoc($sql)){
		echo $data['kode'].$data['judul'];
	}
 ?>
 -->
 <!DOCTYPE html>
<html>
<head>
  <title>e-ticketing Bioskop</title>
  <?php include'asset/head.php';?>
  <style type="text/css">
    .body {
      background-color: #f0f0f5;
    }
    .btn {
      padding: 14px 24px;
      border: 0 none;
      font-weight: 700;
      letter-spacing: 1px;
      text-transform: uppercase;
      font-family: calibri;
    }
    .btn-primary {
     background: #0099cc;
     color: #ffffff;
    }
    .panel-group{
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
    .table{
    	font-size: 16px;
    }
    .page-header{
    	font-size: 36px;
    	font-family: Verdana;
    	text-transform: uppercase;
    	letter-spacing: 3px;
    }
    .video{
    	width: 100%;
    	height: auto;
    }
    .bangku
	{
		width: 30px;
		height:30px;
		text-align:center;
		font-size:20px;
		background-color:#F00;
		color:#FFF;
		cursor:pointer;
		margin-top:4px;
		margin-right:4px;
	}
	
	
	.btn-hijau
	{
		background:#4fb821;
	}		
	
	.btn-biru
	{
		background:#2c35f1;
	}
  </style>
</head>
<body class="body">
<?php include'asset/navbar.php'; ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2"></div>
			<div class="col-md-8">
				<?php 
					include 'asset/koneksi/koneksi.php';
					$kodepage = $_GET['kode'];
					$query = "SELECT * FROM tbl_film WHERE kode='$kodepage' LIMIT 1";
					$sql = mysqli_query($connect, $query);

					while ($data = mysqli_fetch_assoc($sql)){
						echo '<div class="col-md-3">
								<img class="img-responsive" src="asset/img/'.$data['gambar'].'.jpg">
								<br>
							  </div>
							  <div class="col-md-9">
								<div class="page-header">
									<strong>Book : <u>'.$data['judul'].'</u> Ticket</strong>
								</div>';
				?>
						<div class="panel-group">
							<div class="panel panel-info">
								<div class="panel-heading">
									<b>INFORMASI PENONTON</b>
								</div>
								<div class="panel-body">
									<div class="row">
										<form class="form-horizontal" action="act/ins_book.php" method="POST">
											<div class="form-group">
												<label class="control-label col-md-2" for="kodefilm">Kode Film</label>
												<div class="col-md-5">
													<input type="text" class="form-control" name="kodefilm" value="<?php echo $data['kode'];?>" disabled >
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-2" for="nama">Nama</label>
												<div class="col-md-8">
													<input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-2" for="nohp">No. Handphone</label>
												<div class="col-md-8">
													<input type="tel" class="form-control" name="nohp" placeholder="Nomor Handphone">
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-2" for="jam">Jam</label>
												<div class="col-md-3">
													<select class="form-control" name="jam">
														<option value="12:15">12:15</option>
														<option value="14:40">14:40</option>
														<option value="17:05">17:05</option>
														<option value="19:30">19:30</option>
														<option value="21:55">21:55</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="control-label col-md-2" for="jml">Jumlah</label>
												<div class="col-md-3">
													<input type="number" class="form-control" name="jml" placeholder="Jumlah">
												</div>
											</div>
											  <div class="col-sm-1"></div>
											  <div class="col-sm-11">
											  	<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Pilih Kursi</button>
											  </div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="panel-group">
							<div class="panel panel-info">
								<div class="panel-body">
									<a class="btn btn-success btn-block" href="book.php?kode='.$data['kode'].'">Book Now</a>
								</div>
							</div>
						</div>
						<?php }?>
					</div>
			</div>
	</div>
</div>	
<!-- modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
		  <div class="modal-body">
		          <table width="10%" border="0" cellpadding="4" cellspacing="4" class="aaaaa" align="center">
		  <tr>
		    <td colspan="2" align="center" valign="middle"><h5>PINTU</h5></td>
		    <td align="center" valign="middle"><h5>CC</h5></td>
		    <td colspan="2" align="center" valign="middle"><h5>SUPIR</h5></td>
		  </tr>
		  <tr>
		  	<td>A</td>
		    <td><div class="bangku btn-hijau" data-id="0">1</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">2</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">3</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">4</div></td>
		  </tr>
		  <tr>
		  	<td>B</td>
		    <td><div class="bangku btn-hijau" data-id="0">1</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">2</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">3</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">4</div></td>
		  </tr>
		  <tr>
		  	<td>C</td>
		    <td><div class="bangku btn-hijau" data-id="0">1</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">2</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">3</div></td>
		    <td><div class="bangku btn-hijau" data-id="0">4</div></td>
		  </tr>   
		</table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<?php include 'asset/js.php'; ?>
</body>
</html>