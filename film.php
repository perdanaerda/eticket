<!-- <?php 
	include 'asset/koneksi/koneksi.php';
	$kode = $_GET['kode'];
	$query = "SELECT * FROM tbl_film WHERE kode='$kode' LIMIT 1";
	$sql = mysqli_query($connect, $query);

	while ($data = mysqli_fetch_assoc($sql)){
		echo $data['kode'].$data['judul'];
	}
 ?>
 -->
 <!DOCTYPE html>
<html>
<head>
  <title>e-ticketing Bioskop</title>
  <?php include'asset/head.php';?>
  <style type="text/css">
    .body {
      background-color: #f0f0f5;
    }
    .btn {
      padding: 14px 24px;
      border: 0 none;
      font-weight: 700;
      letter-spacing: 1px;
      text-transform: uppercase;
      font-family: calibri;
    }
    .btn-primary {
     background: #0099cc;
     color: #ffffff;
    }
    .panel-group{
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
    .table{
    	font-size: 24px;
    }
    .page-header{
    	font-size: 36px;
    	font-family: Verdana;
    	text-transform: uppercase;
    	letter-spacing: 3px;
    	text-shadow: 4px 8px #e0e0eb;
    	color: red;
    }
    .video{
    	width: 100%;
    	height: auto;
    }
  </style>
</head>
<body class="body">
<?php include'asset/navbar.php'; ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2"></div>
			<div class="col-md-8">
				<?php 
					include 'asset/koneksi/koneksi.php';
					$kode = $_GET['kode'];
					$query = "SELECT * FROM tbl_film WHERE kode='$kode' LIMIT 1";
					$sql = mysqli_query($connect, $query);

					while ($data = mysqli_fetch_assoc($sql)){
						echo '<div class="col-md-3">
								<img class="img-responsive" src="asset/img/'.$data['gambar'].'.jpg">
								<br>
								<div class="panel-group">
									<div class="panel panel-info">
										<div class="panel-heading">
											Informasi Tayang
										</div>
										<div class="panel-body">
											<table border="0" class="table table-responsive">
												<tr>
													<td>Theater</td>
													<td>:</td>
													<td>Mall Boemi Kedaton</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td>Mall Kartini</td>
												</tr>
												<tr>
													<td>Jam</td>
													<td>:</td>
													<td>12:15</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td>14:40</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td>17:05</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td>19:30</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td>21:55</td>
												</tr>
											</table>
										</div>
									</div>
									</div>
							   </div>
					<div class="col-md-9">
						<div class="page-header">
							<strong>'.$data['judul'].'</strong>
						</div>
						<div class="panel-group">
							<div class="panel panel-info">
								<div class="panel-heading">
									<b>SINOPSIS</b>
								</div>
								<div class="panel-body">
									'.$data['sinopsis'].'
								</div>
							</div>
						</div>
						<div class="panel-group">
							<div class="panel panel-info">
								<div class="panel-heading">
									<b>TRAILER</b>
								</div>
								<div class="panel-body">
									<video class="video" controls>
										<source src="asset/video/'.$data['triler'].'.mp4" type="video/mp4">	
									</video>
								</div>
							</div>
						</div>
						<div class="panel-group">
							<div class="panel panel-info">
								<div class="panel-body">
									<a class="btn btn-success btn-block" href="book.php?kode='.$data['kode'].'">Book Now</a>
								</div>
							</div>
						</div>
					</div>';
					}
				 ?>
			</div>
	</div>
	<div class="row">
		<div class="col-md-3">
		</div>
	</div>
</div>	
<?php include 'asset/js.php'; ?>
</body>
</html>