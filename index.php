<!DOCTYPE html>
<html>
<head>
  <title>e-ticketing Bioskop</title>
  <?php include'asset/head.php';?>
  <style type="text/css">
    .body {
      background-color: #f0f0f5;
    }
    .btn {
      padding: 14px 24px;
      border: 0 none;
      font-weight: 700;
      letter-spacing: 1px;
      text-transform: uppercase;
      font-family: calibri;
    }
    .btn-primary {
     background: #0099cc;
     color: #ffffff;
    }
    .panel-group{
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }
  </style>
</head>
<body class="body">
<?php include'asset/navbar.php'; ?>
  <div class="jumbotron text-center">
    <h1>SELAMAT DATANG DI e-Ticketing XXII</h1>
    <h4>Silahkan Pilih Film yang ingin anda saksikan..</h4>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="page-header">
                  <h3>Now Playing..</h3>
        </div>
      </div>
    </div>
    <div class="row">
      <?php 
        include 'asset/koneksi/koneksi.php';

        $query_tampil = "SELECT kode, judul, gambar FROM tbl_film WHERE status=1";
        $tampil = mysqli_query($connect,$query_tampil);

        while ($data = mysqli_fetch_assoc($tampil)) {
          echo '<div class="col-sm-3">
        <div class="panel-group">
          <div class="panel panel-primary">
            <div class="panel-heading">'.$data['judul'].'</div>
            <div class="panel-body" align="center"><img class="img-responsive img-thumbnail" src="asset/img/'.$data['gambar'].'.jpg"></div>
            <div class="panel-body" align="center"><span><a class="btn btn-primary" href="film.php?kode='.$data['kode'].'">Read More</a>  </span><span><a class="btn btn-primary" href="book.php?kode='.$data['kode'].'">Book Now</a>  </span></div>
          </div>
        </div>
      </div>';
        }
      ?>
    </div>
  </div>
<?php include 'asset/js.php'; ?>
</body>
</html>
